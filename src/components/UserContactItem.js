import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { useState } from 'react';


const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  rootLeft: {
    display: "flex",
    minWidth: "350px",
  },
  avatar: {
    height: "55px",
    width: "55px",
      marginRight: theme.spacing(1),
  },
  avatarPlaceholder: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "55px",
    width: "55px",
    backgroundColor: "white",
    borderRadius: "50%",
    border: '2px solid #dcdcdc',
    marginRight: theme.spacing(1),
    "& span": {
      fontSize: "20px",
      color: "#a9a9a9",
    },
  },
  text: {
    textAlign: "left",
  },
  name: {
      color: "#414a4c",
  },
  email: {
      color: "#808080",
  },
  checkbox: {
      width: "45px",
      height: "45px",
  }
}));

const UserContactItem = ({contact, checked, handleClick}) => {
    const classes = useStyles();
    const [isChecked, setIsChecked] = useState(checked);

    const handleCheckbox = (i) => {
        handleClick(i);
        setIsChecked(!isChecked);
    }

    return (
        <div className={classes.root} onClick={(i) => handleCheckbox(i)}>
          <div className={classes.rootLeft}>
            {contact.avatar
            ? <Avatar className={classes.avatar} alt={`${contact.first_name} ${contact.last_name}`} src={contact.avatar}/>
            : <div className={classes.avatarPlaceholder}>
                <span>{`${contact.first_name[0]} ${contact.last_name[0]}`}</span>
              </div>}
            <div className={classes.text}>
                <Typography variant="h5" className={classes.name}>{contact.first_name} {contact.last_name}</Typography>
                <Typography className={classes.email}>{contact.email}</Typography>
            </div>
          </div>
          <Checkbox checked={isChecked} className={classes.checkbox}/>
        </div>
);
}

export default UserContactItem;
