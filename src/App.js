import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import UserContactItem from './components/UserContactItem';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#EFEFEF",
  },
  appBar: {
    "& div": {
      display: "flex",
      justifyContent: "center",
    },
    backgroundImage: "linear-gradient(to bottom right, #5CB2C3, #20A98F)",
  },
  searchInput: {
    backgroundColor: "white",
    '& svg': {
      filter: "invert(83%) sepia(28%) saturate(17%) hue-rotate(24deg) brightness(88%) contrast(94%)",
      marginLeft: theme.spacing(1),
    },
    '& input': {
      height: "40px",
    },
  },
  userContactList: {
    '& > *': {
      paddingBottom: theme.spacing(1),
      borderBottom: "1px solid #dcdcdc",
    },
  },
  showMoreBtn: {
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(2),
  }
}));

const App = () => {
  const classes = useStyles();
  const [userList, setUserList] = useState([]);
  const [selectedUsers] = useState([]);
  const [checkedUsers, setCheckedUsers] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResult, setSearchResult] = useState([]);
  const [showUpTo, setShowUpTo] = useState(9);

  useEffect(() => {
    const fetchData = async () => {
      let res = await (await fetch("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json")).json();
      res.sort((a, b) => a.last_name > b.last_name);
      setUserList(res);
    }
    if(userList.length === 0) fetchData();
    if(searchQuery) {
      const queryText = searchQuery.toLowerCase();
      let result = userList.filter(contact => {
        return contact.first_name.toLowerCase().includes(queryText) || contact.last_name.toLowerCase().includes(queryText);
      })
      setSearchResult(result);
    } else {
      setSearchResult(userList);
    }
  }, [searchQuery, userList])

  const handleUserContactClick = (index) => {
    let checkedUserIndex = checkedUsers.indexOf(index);
    if(checkedUserIndex === -1) {
      checkedUsers.push(index);
      let user = userList.find(u => u.id === index);
      selectedUsers.push(user);
      setCheckedUsers(checkedUsers);
    } else {
      let selectedUserIndex = selectedUsers.map(u => {return u.id}).indexOf(index);
      checkedUsers.splice(checkedUserIndex, 1);
      selectedUsers.splice(selectedUserIndex, 1);
    } 
    console.log(selectedUsers);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.appBar}>
          <Typography edge="center" variant="h4">Contacts</Typography>
        </Toolbar>
      </AppBar>
      <TextField
        fullWidth
        className={classes.searchInput}
        id="search-input"
        value={searchQuery}
        onChange={e => setSearchQuery(e.target.value)}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
      >
      </TextField>
      <div className={classes.userContactList}>
      { searchResult.slice(0, showUpTo).map((contact, i) => (
          <UserContactItem handleClick={() => handleUserContactClick(contact.id)} checked={checkedUsers.includes(i)} contact={contact} key={i} />
        )) }
      </div>
      {searchResult.length - 1 > showUpTo && <Button className={classes.showMoreBtn} variant="contained" color="primary" onClick={() => setShowUpTo(showUpTo + 10)}>Show more...</Button>}
    </div>
  );
}

export default App;

